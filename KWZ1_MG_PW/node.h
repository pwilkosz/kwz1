#pragma once

#include <vector>
#include <iostream>

class graf;

class node
{
public:
	int id;
	int czas_wykonania, ES, EF, LS, LF;
	std::vector<int> krawedzie_we;
	std::vector<int> krawedzie_wy;
	node(int id, int czas_w);
	~node(void);
	void dodaj_krawedz_we(int id);
	void dodaj_krawedz_wy(int id);
	void oblicz_EF_obecnego();
	void oblicz_ES_nastepnego(graf *G);
	void zaproponuj_ES(int es);
};
