#pragma once

#include "node.h"

class graf
{
public:
	std::vector<node> wezly;
	graf(void);
	~graf(void);
	void dodaj_wezel(int id, int czas);
	void usun_wezel(int id); // usuwanie wezla z grafu
	void polacz(int src, int dst);
	int rozmiar();
};