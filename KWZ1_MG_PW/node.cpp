#include "node.h"
#include "graf.h"

node::node(int id, int czas_w)
{
	this->id = id;
	this->czas_wykonania = czas_w;
	ES = EF = LS = LF = 0;
}


node::~node(void)
{
}

void node::dodaj_krawedz_we(int id) {
	krawedzie_we.push_back(id);
}

void node::dodaj_krawedz_wy(int id) {
	krawedzie_wy.push_back(id);
}

void node::oblicz_EF_obecnego() {
	this->EF = ES + czas_wykonania;
}

void node::oblicz_ES_nastepnego(graf *G) {
	for (auto it = krawedzie_wy.begin(); it < krawedzie_wy.end(); it++) {
		G->wezly[*it].zaproponuj_ES(this->EF);
	}
}

void node::zaproponuj_ES(int es) {
	if (es > this->ES) {
		this->ES = es;
	}
}